/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

import java.util.ArrayList;
import java.util.List;

public class WarCardGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
         List<Card> cards = new ArrayList<Card>();
        
        for(CardSuit s : CardSuit.values()){
            for(CardValue v : CardValue.values()){
                Card card = new Card(s, v);
                cards.add(card);
                
                System.out.println(card);
            }
        }
    }
    
}
